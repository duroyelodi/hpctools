#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
//#include "mkl_lapacke.h"

double absolueVal(double val){
  double res;
  if(val < 0){
    res  = -val; 
  }
  else {
    res = val;
      }
  return res; 

}

double *generate_matrix(int size)
{
    int i;
    double *matrix = (double *)malloc(sizeof(double) * size * size);
    srand(1);

    for (i = 0; i < size * size; i++)
    {
      matrix[i] = rand() % 100;
    }

    return matrix; 
}

void print_matrix(const char *name, double *matrix, int size)
{
    int i, j;
    printf("matrix: %s \n", matrix);

    for (i = 0; i < size; i++)
    {
            for (j = 0; j < size; j++)
            {
                printf("%f ", matrix[i * size + j]);
            }
            printf("\n");
    }
}


int check_result(double *bref, double *b, int size) {
  int i;
  double epsilon; 
  epsilon = 0.0005; 

    for(i=0;i<size*size;i++) {
      if (absolueVal(bref[i]-b[i]) > epsilon) return 0; 
    }
    return 1;
}

/*
int my_dgesv(int n, int nrhs, double *a, int lda, int *ipiv, double *b, int ldb) {

    //Replace with your implementation
    LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, nrhs, a, lda, ipiv, b, ldb);

}
*/



/* Gauss Method */

/* Creation of identity matrix */

double *id(int N) {
  int i;
  int j;
  double *mat;

  mat =generate_matrix(N);
  for(i=0; i<N; i++) {
    for(j=0; j<N; j++) {

      if(i == j) {
	       mat[i*N+j] = 1;
      }

      else {
	       mat[i*N+j] = 0;
      }

    }
  }
  return mat;
}


/* multiplication of line with a value  */


void multiplication(double *mat, double *matI, int N, int line, double k) {
    int j;

      for(j=0; j<N; j++) {
	      mat[line*N+j] = k*mat[line*N+j];
        matI[line*N+j] = k*matI[line*N+j];
      }
  }


/* Exchange of 2 lines */

void permutation(double *mat, double *matI, int N, int line1, int line2) {
  int j;
  float tmp;
  float tmpI;
  double start4, end4; 
  tmp = 0;
  tmpI=0;
#pragma omp parallel 
{
  #pragma omp for
    for(j=0; j<N; j++) {

      tmp = mat[line1*N+j];
      //tmpI = matI[line1*N+j];
      mat[line1*N+j] = mat[line2*N+j];
      //matI[line1*N+j] = matI[line2*N+j];
      mat[line2*N+j] = tmp;
      //matI[line2*N+j] = tmpI;
    }
    #pragma omp for
    for(j=0; j<N; j++){
      tmpI = matI[line1*N+j];
      matI[line1*N+j] = matI[line2*N+j];
      matI[line2*N+j] = tmpI;
    }
}
}



/* add line plus mutiplication of another by a value */

void addLineMultiply(double *mat,double *matI, int N, int line1, int line2, double k) {
  int j; 
      for(j=0; j<N; j++) {
        mat[line1*N+j] = mat[line1*N+j] + (k*mat[line2*N+j]);
        matI[line1*N+j] = matI[line1*N+j] + (k*matI[line2*N+j]);
      }
}


/*pivot */

void belowD(double *mat, double *matI, int N, int i) {
  int j; //position 
  if(mat[i*N+i] != 0) {
    multiplication(mat, matI, N, i, 1/mat[i*N+i]);
  }
  else {
    printf("impossible\n");
    exit(-1);
  }

  #pragma omp parallel for 
      for(j=i+1; j<N; j++){
        addLineMultiply(mat, matI, N, j, i, -mat[j*N+i]);
      }
}


/* Triangular shape */

void Triangular(double *mat, double *matI, int N) {
  int i;
  int j;
  
  for(i=0; i<N; i++) {
    if(mat[i*N+i] != 0) {
      belowD(mat, matI, N, i);
    }
    else {
      j = i;
      while(j<=N && mat[i*N+j]==0) {
	      j=j+1;
      }
      if(i>N){
	      printf("impossible\n");
      }
      else {
	      permutation(mat, matI, N, j, i);
      }
      belowD(mat, matI, N, i);
    }
  }
}



/* upper Triangular */

void aboveD(double *mat,double *matI, int N, int i) {
  int j; 
  #pragma omp parallel for
    for(j = i-1; j>=0; j--) {
      #pragma ivdep
      addLineMultiply(mat, matI, N, j, i, -mat[j*N+i]);
    } 
}

/* put 0 below diagonal in order to have diagonal matrix */

void diagonalM(double *mat,double *matI, int N) {
  int i;
  int j;

  for(i = N-1; i>=0; i--) {
    if(mat[i*N+i] != 0) {
      aboveD(mat, matI, N, i);
    }
    else {
      j = i;
	while(j<=0 && mat[i*N+j] == 0) {
	  j= j- 1;
	}
      permutation(mat, matI, N, j, i);
      aboveD(mat, matI, N, i);
    }
  }

}




/* apply functions */

void apply(double *mat, double *matI, int N) {


    belowD(mat, matI, N, 0);

    Triangular(mat, matI, N);

    aboveD(mat, matI, N, N-1);

    diagonalM(mat, matI, N);
    
}




/*! multiply 2 matrix*/

void multiplyMM(double *mat, double *mat1, double *mat2, int N){
  int i;
  int j;
  int k;
  double C;
 
  for(i=0; i<N; i++){
    for(j=0; j<N; j++){
      mat2[i*N+j] = 0.0;
    }
  }
#pragma omp parallel for private(j,k, C) 
  for(i=0; i<N; i++){
    #pragma code_align 32
    for(j=0; j<N; j++){
      C = 0.0;
      for(k=0; k<N; k++){

        C = C + ( mat[i*N+k] * mat1[k*N+j]);

      }
      mat2[i*N+j] = C;
    }
  }
}




void my_dgesv(double *a, double *i, double *b, double *x, int n) {

  //Replace with your implementation  
                                                                                                                                                                           
  apply(a, i, n);


  multiplyMM(i, b, x, n);

  //LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, nrhs, a, lda, ipiv, b, ldb);                                                                                                                                         

}






    void main(int argc, char *argv[])
    {

        int size = atoi(argv[1]);

        double *a, *aref;
        double *b, *bref;
        double *i;
        double *x;
        double deb, fin;
        

        a = generate_matrix(size);
        aref = generate_matrix(size);
        b = generate_matrix(size);
        bref = generate_matrix(size);
        i = id(size);
        x = generate_matrix(size);

        //print_matrix("A", a, size);
        //print_matrix("B", b, size);

        // Using MKL to solve the system
        //MKL_INT n = size, nrhs = size, lda = size, ldb = size, info;
        //MKL_INT *ipiv = (MKL_INT *)malloc(sizeof(MKL_INT)*size);

        clock_t tStart = clock();
        //info = LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, nrhs, aref, lda, ipiv, bref, ldb);
        //printf("Time taken by MKL: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);

        tStart = clock();
        deb = omp_get_wtime(); 
        //MKL_INT *ipiv2 = (MKL_INT *)malloc(sizeof(MKL_INT)*size);

        //my_dgesv(n, nrhs, a, lda, ipiv2, b, ldb);
        
        my_dgesv(a, i, b, x, size);
        fin = omp_get_wtime(); 
        printf("Time taken by my implementation: %.2fs\n", (double)(clock() - tStart) / CLOCKS_PER_SEC);
        printf("Time taken by my implementation: %fs\n", (fin -deb));

/*
        if (check_result(bref,x,size)==1)
            printf("Result is ok!\n");
        else
            printf("Result is wrong!\n");
  
         //print_matrix("X", x, size);
        //print_matrix("Xref", bref, size);
        */
        
      //free(ipiv); 
      //free(ipiv2);
      free(a); 
      free(aref); 
      free(b); 
      free(bref); 
      free(x); 
      free(i); 

    }
